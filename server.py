from concurrent import futures
import os
# GRPC
import grpc
from denoise_pb2 import WavFileBinary
import denoise_pb2_grpc
# ENGIN
from engine import pack_model
from engine import pack_audio_utils

CHUNK_SIZE = 1024 * 1024  # 1MB

def read_bytes(file_, num_bytes):
    while True:
        bin = file_.read(num_bytes)
        if len(bin) == 0:
            break
        yield bin


class DenoiseServicer(denoise_pb2_grpc.DenoiseServicer):
    def __init__(self,args):
        self.pack = pack_model.packed('checkpoint/masking_epoch=126.ckpt', None, args, None)

    def StreamDenoise(self, request_iterator, context):
        print('Start StreamDenoise')
        mag, phase = self.pack.SpecFromWav(request_iterator)
        masked = self.pack.GetMaskFromSpec(mag)
        wav_masked = self.pack.ReconFromMaskedSpec(masked, phase, mag, 1.)
        wav_bytes = self.pack.WavToBytes(wav_masked)
        for i in range((len(wav_bytes) -1)//CHUNK_SIZE + 1):
            yield WavFileBinary(bin = wav_bytes[CHUNK_SIZE *i: CHUNK_SIZE*(i+1)])
        print('End StreamDenoise')
        del mag
        del phase
        del masked
        del wav_masked
        del wav_bytes
        self.pack.EmptyCache()
        
def serve(args):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    denoise_pb2_grpc.add_DenoiseServicer_to_server(DenoiseServicer(args), server)
    print('Starting server. Listening on port 5001.')
    server.add_insecure_port('[::]:5001')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--device', type=int, default = 0, help ="device")
    args = parser.parse_args()
    serve(args)
