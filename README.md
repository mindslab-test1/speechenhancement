# ENV
 - PORT = 9111
# CMD
## Init Local
```
pip install -r requirements.txt
```
## Run Grpc Server
```
python server.py
```
## Build Proto File
```
python -m grpc_tools.protoc -I ./proto --python_out=. --grpc_python_out=. denoise.proto
```
# TEST
## Run Grpc Client
```
cd test
python client.py
```
## Sample .Wav
```
kor8000.wav [samplerate: 8000, channels: mono, duration: 3s, size: 112.9kb]
kor16000.wav [samplerate: 16000, channels: mono, duration: 3s, size: 112.9kb]
```
# Docker
## Build
```
docker build -t denoise:1.1.1 .
```
##Pull
```
docker pull docker.maum.ai:443/brain/denoise
```
## Run
```
docker run --runtime=nvidia -it --name denoise -p 3000:5001 -d denoise:1.1.1
```
# Author
Engine developed by 이준혁
GRPC packaged by 유종호
