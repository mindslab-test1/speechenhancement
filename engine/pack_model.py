import numpy as np
import torch
import torch.nn as nn
#for packed
from omegaconf import OmegaConf as OC
from engine import pack_audio_utils as au
import librosa
from scipy.io.wavfile import read

class MaskingModel(nn.Module):
    def __init__(self, hparam):
        super(MaskingModel, self).__init__()
        self.hparam = hparam
        #original conv at paper
        self.conv = nn.Sequential(
                nn.Conv2d(1,48,(1, 7),padding = (0, 3)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(7, 1),padding = (3, 0)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (2, 2), dilation = (1, 1)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (4, 2), dilation = (2, 1)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (8, 2), dilation = (4, 1)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (16, 2), dilation = (8, 1)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (2, 2), dilation = (1, 1)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (4, 4), dilation = (2, 2)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (8, 8), dilation = (4, 4)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,48,(5, 5),padding = (16, 16), dilation = (8, 8)),
                nn.BatchNorm2d(48), nn.ReLU(),

                nn.Conv2d(48,1,(1, 1),padding = (0, 0)),
                nn.BatchNorm2d(1), nn.Sigmoid()
                )

    def forward(self,x): #x: [B, F, T]
        x= x.unsqueeze(1) # x: [B, 1, F, T]
        mask= self.conv(x) #mask: [B, 1, F, T]
        x= mask*x
        x= x.squeeze(1) #x: [B, F, T]
        return x

    def load(self, ckpt_path):
        ckpt = torch.load(ckpt_path)
        self.load_state_dict(ckpt['state_dict'], strict = False)
        del ckpt
        return

class packed(object):
    def __init__(self, ckpt_path,hp_path, args, logger):
        # ujon: File path is changed
        ckpt_path = 'checkpoint/masking_epoch=126.ckpt' if ckpt_path==None else ckpt_path
        hp_path = 'checkpoint/hparameter.yaml' if hp_path==None else hp_path
        self.hparam = OC.load(hp_path)
        self.device = args.device
        self.model = MaskingModel(self.hparam).to(self.device)
        self.model.load(ckpt_path)
        self.model.eval()
        torch.cuda.set_device(self.device)

        self.audio = au.torch_audio(self.hparam,device = self.device)
        self.wavmaker = au.WavMaker(self.hparam.audio.sampling_rate)
        torch.cuda.empty_cache()

    def EmptyCache(self):
        torch.cuda.set_device(self.device)
        torch.cuda.empty_cache()
        return
    
    def SpecFromWav(self, wav_binary_iterator):
        torch.cuda.set_device(self.device)
        with torch.no_grad():
            wav = bytearray()
            for wav_binary in wav_binary_iterator:
                wav.extend(wav_binary.bin)

            wav = au.WavBinaryWrapper(wav)
            sampling_rate, wav = read(wav)

            if len(wav.shape) == 2:
                wav = wav[:, 0]

            if wav.dtype == np.int16:
                wav = wav / 32768.0
            elif wav.dtype == np.int32:
                wav = wav / 2147483648.0
            elif wav.dtype == np.uint8:
                wav = (wav - 128) / 128.0

            wav = wav.astype(np.float32)
            if sampling_rate != self.hparam.audio.sampling_rate:
                wav = librosa.resample(wav, sampling_rate,
                                       self.hparam.audio.sampling_rate)
                wav = np.clip(wav, -1.0, 1.0)

            mag, phase = self.audio.stft_(wav)
        return mag, phase

    #For test
    def SpecFromWav_Test(self, wav_file):
        torch.cuda.set_device(self.device)
        wav = self.audio.load(wav_file)
        mag, phase = self.audio.stft_(wav)
        return mag, phase

    def GetMaskFromSpec(self, mag):
        torch.cuda.set_device(self.device)
        masked = []
        time_length = mag.shape[-1]
        with torch.no_grad():
            for i in range(time_length//self.hparam.serve.max_t +\
                    (time_length%self.hparam.serve.max_t != 0) ):
                masked.append(self.model(mag[...,self.hparam.serve.max_t*i:\
                        min(self.hparam.serve.max_t *(i+1), time_length) ].unsqueeze(0)).squeeze(0))
            masked = torch.cat(masked, dim = -1)
        return masked

    def ReconFromMaskedSpec(self, masked, phase, mag, z=1):
        torch.cuda.set_device(self.device)
        new_mag = masked*z + mag*(1-z)
        wav_masked = self.audio.istft_pow_(new_mag, phase)
        return wav_masked
    
    def WavToBytes(self, wavdata):
        wavlist = wavdata.cpu().detach().numpy()
        wavint = np.int16(wavlist/(np.max(np.absolute(wavlist)) + 0.001) *32767 )
        wav_bytes = self.wavmaker.make_header(len(wavint))
        wav_bytes += self.wavmaker.make_data(wavint)
        return wav_bytes


# if __name__ =='__main__':
#     import argparse
#     parser = argparse.ArgumentParser()
#     parser.add_argument('-d', '--device', type=int, default = 0, help ="device")
#     args = parser.parse_args()
#     #Init
#     pack_test = packed('checkpoint/masking_epoch=126.ckpt', None, args, None)
#     ###
#     mag, phase = pack_test.SpecFromWav_Test("DUDUDUNGA.wav")#sample
#     masked = pack_test.GetMaskFromSpec(mag)
#     wav_masked = pack_test.ReconFromMaskedSpec(masked, phase, mag, 0.5)
#     ###
#     #for test
#     pack_test.audio.wav_save(wav_masked, 'pack_test.wav')
#     exit()
