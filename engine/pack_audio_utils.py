import librosa as rosa
import numpy as np
import torch
from torchaudio.functional import istft
import sys
import struct

class torch_audio():
    def __init__(self,hparam, device=None):
        self.hparam = hparam
        self.hann_window = torch.hann_window(self.hparam.audio.n_fft, device = device if device!=None \
                else None)
        self.gpu = True if device else False

    def load(self,filepath,mono = True):
        y,_ = rosa.load(filepath, sr = self.hparam.audio.sampling_rate, mono =mono)
        return y

    def _tensor_check(self,array):
        if type(array) ==type(np.array([])):
            array= torch.tensor(array, device= self.hann_window.device)
        elif type(array) == type(torch.tensor([])):
            if self.gpu and array.device !=self.hann_window.device:
                array.to(self.hann_window.device)
        else:
            assert False, "input is not np.array nor torch.tensor"
        return array

    def stft_mag_pow(self,y):
        y = self._tensor_check(y)
        D = torch.norm(torch.stft(y, n_fft=  self.hparam.audio.n_fft, 
            hop_length = self.hparam.audio.hop_length, 
            window = self.hann_window), p=2, dim=-1)
        mag = torch.pow(D, 0.3)
        return mag

    def stft_phase(self,y):
        y = self._tensor_check(y)
        D = torch.stft(y, n_fft=  self.hparam.audio.n_fft, 
            hop_length = self.hparam.audio.hop_length, 
            window = self.hann_window)
        n = torch.norm(D,p = 2, dim = -1).unsqueeze(-1)+torch.finfo().eps
        phase = D/n
        return phase
    def stft_(self, y):
        return self.stft_mag_pow(y), self.stft_phase(y)

    def istft_pow_(self, mag_pow, phase):
        mag = torch.pow(mag_pow, 10./3)
        D = phase*mag.view([mag.shape[0], mag.shape[1],1])
        assert D.shape == phase.shape
        y = istft(D, n_fft = self.hparam.audio.n_fft,
                hop_length = self.hparam.audio.hop_length,
                window = self.hann_window)
        return y
    def wav_save(self, wav, wav_path):
        if type(wav) == torch.Tensor:
            wav = wav.cpu().detach().numpy()
        rosa.output.write_wav(wav_path, wav, sr = self.hparam.audio.sampling_rate)
        return


class WavBinaryWrapper(object):
    def __init__(self, data):
        super(WavBinaryWrapper, self).__init__()
        self.data = data
        self.position = 0

    def read(self, n=-1):
        if n == -1:
            data = self.data[self.position:]
            self.position = len(self.data)
            return data
        else:
            data = self.data[self.position:self.position+n]
            self.position += n
            return data

    def seek(self, offset, whence=0):
        if whence == 0:
            self.position = offset
        elif whence == 1:
            self.position += offset
        elif whence == 2:
            self.position = len(self.data) + offset
        else:
            raise RuntimeError('test')
        return self.position

    def tell(self):
        return self.position

    def close(self):
        pass



class WavMaker:
    def __init__(self, sample_rate = None, channels=1, dtype=np.dtype(np.int16)):
        self.dtype = dtype
        self.channels = channels
        self.sample_rate = sample_rate if sample_rate is not None else 16000
        self.WAVE_FORMAT_PCM = 0x0001
        self.WAVE_FORMAT_IEEE_FLOAT = 0x0003

    def make_header(self, sample_length):
        nbytes = sample_length * self.channels * self.dtype.itemsize
        dkind = self.dtype.kind
        if not (dkind == 'i' or dkind == 'f' or (dkind == 'u' and
                                                 self.dtype.itemsize == 1)):
            raise ValueError("Unsupported data type '%s'" % self.dtype)

        header_data = b''

        header_data += b'RIFF'
        header_data += struct.pack('<I', nbytes + 36)
        header_data += b'WAVE'

        # fmt chunk
        header_data += b'fmt '
        if dkind == 'f':
            format_tag = self.WAVE_FORMAT_IEEE_FLOAT
        else:
            format_tag = self.WAVE_FORMAT_PCM
        bit_depth = self.dtype.itemsize * 8
        bytes_per_second = self.sample_rate*(bit_depth // 8)*self.channels
        block_align = self.channels * (bit_depth // 8)

        fmt_chunk_data = struct.pack('<HHIIHH', format_tag, self.channels, self.sample_rate,
                                     bytes_per_second, block_align, bit_depth)
        if not (dkind == 'i' or dkind == 'u'):
            # add cbSize field for non-PCM files
            fmt_chunk_data += b'\x00\x00'

        header_data += struct.pack('<I', len(fmt_chunk_data))
        header_data += fmt_chunk_data

        # fact chunk (non-PCM files)
        if not (dkind == 'i' or dkind == 'u'):
            header_data += b'fact'
            header_data += struct.pack('<II', 4, sample_length)

        # check data size (needs to be immediately before the data chunk)
        if ((len(header_data)-4-4) + (4+4+nbytes)) > 0xFFFFFFFF:
            raise ValueError("Data exceeds wave file size limit")

        header_data += b'data'
        header_data += struct.pack('<I', nbytes)
        return header_data

    def make_data(self, data):
        if self.dtype.byteorder == '>' or (self.dtype.byteorder == '=' and
                                           sys.byteorder == 'big'):
            data = data.byteswap()
        return data.ravel().view('b').data

    
