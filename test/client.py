import sys
sys.path.append('..')
import grpc
from denoise_pb2 import WavFileBinary
from denoise_pb2_grpc import DenoiseStub

CHUNK_SIZE = 1024 * 1024  # 1MB


def read_bytes(file_, num_bytes):
    while True:
        bin = file_.read(num_bytes)
        if len(bin) == 0:
            break
        yield bin


def read_test_file():
    with open('kor8000.wav', 'rb') as f:
        for byte in read_bytes(f, CHUNK_SIZE):
            yield WavFileBinary(bin=byte)


if __name__ == '__main__':
    with grpc.insecure_channel("127.0.0.1:5001") as channel:
        stub = DenoiseStub(channel)
        outputs = stub.StreamDenoise(read_test_file())
        print('OUTPUT')
        for output in outputs:
            print(output.bin)