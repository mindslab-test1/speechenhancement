FROM docker.maum.ai:443/brain/denoise:1.1.1

WORKDIR /app

COPY . .
# RUN pip install -r requirements_docker.txt
RUN python -m grpc_tools.protoc -I ./proto --python_out=. --grpc_python_out=. denoise.proto

EXPOSE 5001

CMD ["python","server.py"]
